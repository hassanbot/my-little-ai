import copy
import math

import gym

from martai import Population
from martai import NeuralNet

# Scene specific parameters
n_outputs = 2
n_inputs = 4

# Variable parameters
n_individuals = 10
n_hidden = 5
n_epochs = 200
n_repetitions = 100
mutate_rate = 0.05
mutate_strength = 10
child_rate = 0.1
d_remove = 0.5

# Calculated parameters
n_weights = (n_inputs+1)*n_hidden + (n_hidden+1)*n_outputs

# Intitialize population
population = Population(n_individuals, n_weights,
                        crossover_frequency=0.8,
                        gene_min=-10.0, gene_max=10.0)

# Initialize environment
env = gym.make('CartPole-v0')
net = NeuralNet(n_inputs, n_hidden, n_outputs)


def run_scene(v_env, v_net, render=False, repetitions=1):
    reward_tot = 0.0
    for rep in xrange(repetitions):
        observation = v_env.reset()
        for t in range(100):
            if render:
                v_env.render()
            action = v_net.feed_forward(observation)
            observation, reward, done, info = v_env.step(action)
            reward_tot += reward
            if done:
                break
    return reward_tot


for epoch in xrange(n_epochs):
    for i, individual in enumerate(population.m_individuals):
        net.set_weights(copy.copy(individual.m_genome))
        individual.m_fitness = run_scene(env, net, False, n_repetitions)

    population.sort()
    population.print_fitness()
    print("Epoch " + str(epoch) + " finished.")
    print("Best: {}; fitness = {}".format(population[0].id, population[0].m_fitness) + ".")
    net.set_weights(copy.copy(population[0].m_genome))
    run_scene(env, net, True)
    population.evolve(int(math.floor(n_individuals*d_remove)),
                      mutate_rate=mutate_rate, mutate_strength=mutate_strength)
