import numpy as np
import matplotlib.pyplot as plt
from NetGraph import NetGraph
import copy

# Raise error for overflow warnings
np.seterr(over='raise')


# Sigmoid activation function
def sigmoid(x):
    if x > 100:
        return 1
    elif x < -100:
        return 0
    else:
        return 1 / (1 + np.exp(-x))


# Class to simulate a neural net
class NeuralNet:
    def __init__(self, num_inputs, num_hidden, num_outputs, visualize=False):
        # Number of nodes
        self.m_numinputs = num_inputs + 1  # Add 1 as bias input to input layer
        self.m_numhidden = num_hidden + 1  # Add 1 as bias input to hidden layer
        self.m_numoutputs = num_outputs

        # Initialize nodes
        self.m_input = np.ones(self.m_numinputs)
        self.m_hidden = np.ones(self.m_numhidden)
        self.m_output = np.ones(self.m_numoutputs)

        # Initialize weights (this will be changed to an Individual later)
        # m_wi[input][hidden]
        # m_wo[hidden][output]
        self.m_wi = np.random.randn(self.m_numinputs, self.m_numhidden-1)
        self.m_wo = np.random.randn(self.m_numhidden, self.m_numoutputs)

        # Visualization with networkx and matplotlib.pyplot
        self.visualize = visualize
        if self.visualize:
            plt.ion()
            self.fig, _ = plt.subplots()
            self.graph = NetGraph()
            self.graph.add_layer('input', self.m_input, [])
            self.graph.add_layer('hidden', self.m_hidden, self.m_wi)
            self.graph.add_layer('output', self.m_output, self.m_wo)

    # Do a feed-forward step
    def feed_forward(self, inputs):
        if len(inputs) != self.m_numinputs-1:
            raise ValueError("Wrong number of inputs.")

        # Initialize input nodes
        for i in xrange(self.m_numinputs-1):
            self.m_input[i] = inputs[i]

        # Activate hidden layer
        for j in xrange(self.m_numhidden-1):
            v_sum = 0.0
            for i in xrange(self.m_numinputs):
                v_sum += self.m_input[i] * self.m_wi[i][j]
            self.m_hidden[j] = (sigmoid(v_sum)*2)-1

        # Activate output layer
        for k in xrange(self.m_numoutputs):
            v_sum = 0.0
            for j in xrange(self.m_numhidden):
                v_sum += self.m_hidden[j] * self.m_wo[j][k]
                # print("v_sum, o = {}, h = {}: {}".format(k, j, v_sum))
            self.m_output[k] = sigmoid(v_sum)

        # # Return output layer after activation
        # return self.m_output[:]
        # Return index of highest output

        if self.visualize:
            self.graph.update_nodes_in_layer(0, self.m_input)
            self.graph.update_nodes_in_layer(1, self.m_hidden)
            self.graph.update_nodes_in_layer(2, self.m_output)

        return self.m_output.argmax()

    def draw_graph(self):
        self.fig.clear()
        self.graph.draw_graph()
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    # Set weights from a list
    def set_weights(self, weights):
        weights = copy.copy(weights)
        n_weights_in = len(weights)
        n_weights = sum(len(x) for x in self.m_wi) + sum(len(x) for x in self.m_wo)
        if n_weights_in != n_weights:
            raise ValueError("Wrong number of weights in array (got " + str(n_weights_in) +
                             ", wants " + str(n_weights) + ").")

        # Set weights for inputs->hidden
        for i in xrange(self.m_numinputs):
            for h in xrange(self.m_numhidden-1):
                self.m_wi[i][h] = weights.pop()

        # Set weights for hidden->output
        for h in xrange(self.m_numhidden):
            for o in xrange(self.m_numoutputs):
                self.m_wo[h][o] = weights.pop()

        if weights:
            raise ValueError("Input array is not empty after weight assignment")
