import copy  # copy(): For copying genome when inheriting from one parent
import itertools  # count(): For creating a unique ID
import math  # floor() and ceil(): For flooring and ceiling when splitting an array in 2
import random  # random(): For creating random genomes and mutating


class Individual:
    newid = itertools.count().next

    def __init__(self, num_genes,
                 parents=None, crossover_frequency=1.0, gene_specs=None,
                 min_value=-10.0, max_value=10.0):

        self.id = Individual.newid()
        self.m_fitness = 0.0
        self.parents = None
        self.inheritance = None

        # Genome is a list of values between min_value and max_value
        self.m_genome = []

        # If 1 parent is given, initialize as a copy of that individual
        # If 2 parents are given, initialize genes as combination of parents genes
        #   If gene_spec is given, choose genes from parents according to this list
        #   Otherwise create genome randomly based on the crossover frequency
        # If no parents are given, initialize with random genes
        if parents:
            if len(parents) == 1:
                self.m_genome = copy.copy(parents[0].m_genome)
                self.parents = [parents]
                self.inheritance = [0] * num_genes
            elif len(parents) < 3:
                if gene_specs:
                    self.inheritance = gene_specs
                    for i, g in enumerate(gene_specs):
                        self.m_genome.append(parents[g].m_genome[i])
                else:
                    self.inheritance = []
                    for i in xrange(num_genes):
                        if random.random() < crossover_frequency:
                            self.m_genome.append(parents[0].m_genome[i])
                            self.inheritance.append(0)
                        else:
                            self.m_genome.append(parents[1].m_genome[i])
                            self.inheritance.append(1)

        else:
            for g in xrange(0, num_genes):
                self.m_genome.append(random.uniform(min_value, max_value))

    def mutate(self, rate=0.05, strength=10.0):
        for i in xrange(0, len(self.m_genome)):
            if random.random() < rate:
                self.m_genome[i] *= (1 + random.uniform(-strength, strength))

    def __eq__(self, other):
        if (self.m_genome == other.m_genome) \
                and (self.m_fitness == other.m_fitness):
            return True
        else:
            return False


class Population:
    RANDOM = 0
    SINGLE_PARENT = 1
    TWO_PARENTS_ONE_CHILD = 2
    TWO_PARENTS_TWO_CHILDREN = 3

    def __init__(self, num_individuals, num_genes, crossover_frequency=1.0,
                 gene_min=-10.0, gene_max=10.0):
        self.m_individuals = []
        self.m_numgenes = num_genes
        self.m_crossfreq = crossover_frequency
        self.m_maxval = gene_max
        self.m_minval = gene_min
        for i in xrange(num_individuals):
            self.m_individuals.append(Individual(num_genes,
                                                 min_value=gene_min,
                                                 max_value=gene_max))

    def mutate(self, rate=0.05, strength=10.0):
        for i in xrange(len(self.m_individuals)):
            self.m_individuals[i].mutate(rate, strength)

    def add_child(self, parent1=None, parent2=None, add_two=False):
        """Adds """
        if parent1 and parent2:
            self.m_individuals.append(Individual(self.m_numgenes,
                                                 parents=[parent1, parent2],
                                                 crossover_frequency=self.m_crossfreq))
            if add_two:
                inv_inherited = []
                for i in self[-1].m_inheritance:
                    inv_inherited.append(1-i)
                self.m_individuals.append(Individual(self.m_numgenes,
                                                     parents=[parent1, parent2],
                                                     gene_specs=inv_inherited))

        elif parent1 or parent2:
            parent = parent1 if parent1 else parent2
            self.m_individuals.append(Individual(self.m_numgenes,
                                                 parents=[parent],
                                                 crossover_frequency=self.m_crossfreq))
        else:
            self.m_individuals.append(Individual(self.m_numgenes,
                                                 min_value=self.m_minval,
                                                 max_value=self.m_maxval))

    def sort(self):
        self.m_individuals.sort(key=lambda x: x.m_fitness, reverse=True)

    def evolve(self, num_remove, child_rate=0.5, mutate_rate=0.05, mutate_strength=10.0, strategy=RANDOM):
        self.sort()
        # Remove worst children
        for i in xrange(num_remove):
            self.m_individuals.pop()

        # Add children according to strategy
        if strategy == Population.RANDOM:
            for i in xrange(int(math.ceil(num_remove * child_rate))):
                self.add_child()
        elif strategy == Population.SINGLE_PARENT:
            for i in xrange(int(math.ceil(num_remove * child_rate))):
                self.add_child(self[i])
        elif strategy == Population.TWO_PARENTS_ONE_CHILD:
            for i in xrange(int(math.ceil(num_remove * child_rate))):
                self.add_child(self.m_individuals[i], self.m_individuals[i + 1])
        # elif strategy == Population.TWO_PARENTS_TWO_CHILDREN:
        #     for i in xrange(int(math.ceil(num_remove * child_rate))):
        #         self.add_child(self.m_individuals[i], self.m_individuals[i + 1])
        else:
            raise ValueError("Unknown strategy: " + str(strategy))

        # Add rest as random new individuals
        for i in xrange(int(math.floor(num_remove * (1 - child_rate)))):
            self.add_child()
        # Mutate
        for i in xrange(len(self.m_individuals)):
            self.m_individuals[i].mutate(mutate_rate, mutate_strength)

    def print_fitness(self):
        for individual in self.m_individuals:
            print("{}: {}".format(individual.id, individual.m_fitness))

    def __getitem__(self, item):
        return self.m_individuals[item]

    def __eq__(self, other):
        if self.m_individuals == other.m_individuals \
                and self.m_numgenes == other.m_numgenes \
                and self.m_crossfreq == other.m_crossfreq \
                and self.m_maxval == other.m_maxval \
                and self.m_minval == other.m_minval:
            return True
        else:
            return False
