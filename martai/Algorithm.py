import copy

from martai import RobotPopulation
import gym

# Logging
import logging


# logging.basicConfig(level=logging.INFO, format='[%(levelname)s]: %(message)s')


# A class to run the algorithm
def run_algorithm(env_name, net_params, pop_params, rep_params, epochs_between_render=1, pause=0.0):
    """Runs the algorithm. The inputs are dicts:
    'net_params' = (n_inputs, n_hidden, n_outputs).
    'pop_params' = (init, mutate, evolve).
        'init' = (n_individuals, gene_min, gene_max).
        'mutate' = (rate, strengths).
        'evolve' = (remove_rate, parent_rate, crossover_rate, strategy).
    'rep_params' = (n_epochs, n_repetitions, n_timesteps).
    Where
    'n_epochs' is the number of times the whole population is trained and evolved.
    'n_repetitions' is the number of times an environment is run for each individual and epoch.
    'n_timesteps' is the number of timesteps the simulated environment is ran for."""
    n_epochs = rep_params['n_epochs']

    # Intitialize population
    population = RobotPopulation(env_name, net_params, pop_params, rep_params)

    epoch_number = 0
    env = gym.make(env_name)
    for epoch in xrange(n_epochs):
        population.reset_fitness()
        population.run_epoch()
        population.evolve()

        # Print info
        if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
            population.print_fitness()
        logging.info("Epoch " + str(epoch) + " finished.")
        # logging.info("Best: {}; fitness = {}".format(population[0].id, population[0].individual.m_fitness) + ".")
        logging.info("ID: {}; "
                     "current fitness = {}; "
                     "mean fitness = {}; "
                     "age = {}"
                     .format(population[0].id,
                             population[0].individual.m_fitness,
                             population[0].mean_fitness,
                             population[0].age) + ".")

        # Render environment for best individual
        if epoch_number >= epochs_between_render:
            best_fitness = population[0].run_scene(env=env, render=True, pause=pause)
            logging.info("Test scene reward for ID " + str(population[0].id) + " = " +
                         str(best_fitness))
            epoch_number = 0

        epoch_number += 1

    population.print_fitness()
