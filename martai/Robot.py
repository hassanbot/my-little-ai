import collections

from .Genetics import Individual  # Each robot carries an Individual
from .NeuralNet import NeuralNet  # Each robot carries a NeuralNet
from .Genetics import Population  # Strategy variables

import gym  # Gym environment

import math  # floor and ceil in evolve
import time  # pause for rendering scenes
import threading  # For parallelizing epoch runs


class Robot:
    def __init__(self, robot_params, individual=None):

        # Initialize variables
        self.net_params = robot_params['net_params']
        self.pop_params = robot_params['pop_params']
        self.rep_params = robot_params['rep_params']

        # Initialize environment
        self.env = gym.make(robot_params['env_name'])

        # Extract parameter information
        n_inputs = self.net_params['n_inputs']
        n_hidden = self.net_params['n_hidden']
        n_outputs = self.net_params['n_outputs']

        if not individual:
            num_genes = (n_inputs + 1) * n_hidden + (n_hidden + 1) * n_outputs
            gene_min = self.pop_params['init']['gene_min']
            gene_max = self.pop_params['init']['gene_max']

            # Create random individual
            self.individual = Individual(num_genes, min_value=gene_min, max_value=gene_max)
        else:
            self.individual = individual

        self.id = self.individual.id
        self.mean_fitness = 0.0
        self.age = 0

        # Create net
        self.net = NeuralNet(n_inputs, n_hidden, n_outputs)
        self.net.set_weights(self.individual.m_genome)

    def run_scene(self, env=None, render=False, pause=None):
        if not env:
            env = self.env
        observation = env.reset()
        reward_tot = 0
        for t in range(self.rep_params['n_timesteps']):
            if render:
                env.render()
            action = self.net.feed_forward(observation)
            observation, reward, done, info = env.step(action)
            reward_tot += reward
            if pause:
                time.sleep(pause)
            if done:
                break
        self.individual.m_fitness += reward_tot
        self.mean_fitness = (self.mean_fitness*self.age + reward_tot) / float(self.age+1)
        self.age += 1
        return reward_tot

    def run_epoch(self):
        return threading.Thread(target=self.run_scene)

    def __lt__(self, other):
        return self.individual.m_fitness < other.individual.m_fitness


class RobotPopulation(collections.MutableSequence):
    def __len__(self):
        return len(self._robots)

    def __getitem__(self, index):
        return self._robots[index]

    def __setitem__(self, index, value):
        self._robots[index] = value

    def insert(self, index, value):
        self._robots.insert(index, value)

    def __delitem__(self, index):
        del self._robots[index]

    def sort(self):
        self._robots.sort(key=lambda x: x.individual.m_fitness, reverse=True)

    def sort_age(self):
        self._robots.sort(key=lambda x: x.age, reverse=True)

    def sort_mean_fitness(self):
        self._robots.sort(key=lambda x: x.mean_fitness, reverse=True)

    def __init__(self, env_name, net_params, pop_params, rep_params):
        self._robots = list()

        self.net_params = net_params
        self.pop_params = pop_params
        self.rep_params = rep_params

        self.robot_params = {'env_name': env_name,
                             'net_params': net_params,
                             'pop_params': pop_params,
                             'rep_params': rep_params}

        # Extract parameter information
        n_inputs = self.net_params['n_inputs']
        n_hidden = self.net_params['n_hidden']
        n_outputs = self.net_params['n_outputs']

        self.num_genes = (n_inputs + 1) * n_hidden + (n_hidden + 1) * n_outputs
        gene_min = self.pop_params['init']['gene_min']
        gene_max = self.pop_params['init']['gene_max']
        self.m_crossfreq = self.pop_params['evolve']['crossover_rate']
        self.m_maxval = gene_max
        self.m_minval = gene_min
        for i in xrange(self.pop_params['init']['n_individuals']):
            self.append(Robot(self.robot_params))

    def mutate(self):
        """Mutates the population with specified rate and strength"""
        for robot in self:
            robot.individual.mutate(self.pop_params['mutate']['rate'], self.pop_params['mutate']['strength'])
            robot.net.set_weights(robot.individual.m_genome)

    def add_child(self, parent1=None, parent2=None, add_two=False):
        """Adds a new robot child to the population"""
        # Add child from two parents
        if parent1 and parent2:
            individual = Individual(self.num_genes,
                                    parents=[parent1, parent2],
                                    crossover_frequency=self.m_crossfreq)
            if add_two:
                inv_inherited = []
                for i in self[-1].individual.m_inheritance:
                    inv_inherited.append(1 - i)
                individual = Individual(self.num_genes,
                                        parents=[parent1, parent2],
                                        gene_specs=inv_inherited)

        # Add child from one parent
        elif parent1 or parent2:
            parent = parent1 if parent1 else parent2
            individual = Individual(self.num_genes,
                                    parents=[parent],
                                    crossover_frequency=self.m_crossfreq)

        # Add random child without parents
        else:
            individual = Individual(self.num_genes,
                                    min_value=self.m_minval,
                                    max_value=self.m_maxval)

        self.append(Robot(self.robot_params, individual))

    def evolve(self):
        self.sort()

        # Initialize helper variables
        num_remove = int(math.floor(self.pop_params['init']['n_individuals']*self.pop_params['evolve']['remove_rate']))
        child_rate = self.pop_params['evolve']['parent_rate']
        strategy = self.pop_params['evolve']['strategy']

        # Remove worst children
        for i in xrange(num_remove):
            self.pop()

        # Add children according to strategy
        if strategy == Population.RANDOM:
            for i in xrange(int(math.ceil(num_remove * child_rate))):
                self.add_child()
        elif strategy == Population.SINGLE_PARENT:
            for i in xrange(int(math.ceil(num_remove * child_rate))):
                self.add_child(self._robots[i].individual)
        elif strategy == Population.TWO_PARENTS_ONE_CHILD:
            for i in xrange(int(math.ceil(num_remove * child_rate))):
                self.add_child(self._robots[i].individual, self._robots[i + 1].individual)
        # elif strategy == Population.TWO_PARENTS_TWO_CHILDREN:
        #     for i in xrange(int(math.ceil(num_remove * child_rate))):
        #         self.add_child(self.m_individuals[i], self.m_individuals[i + 1])
        else:
            raise ValueError("Unknown strategy: " + str(strategy))

        # Add rest as random new individuals
        for i in xrange(int(math.floor(num_remove * (1 - child_rate)))):
            self.add_child()
        # Mutate
        self.mutate()

    def reset_fitness(self):
        for robot in self._robots:
            robot.individual.m_fitness = 0

    def run_epoch(self, use_threading=False):
        """Run an epoch for each robot"""

        if use_threading:
            threads = []
            for robot in self._robots:
                threads.append(threading.Thread(target=robot.run_scene))
                threads[-1].start()

            for thread in threads:
                thread.join()
        else:
            for robot in self._robots:
                robot.run_scene()

    def print_fitness(self):
        self.sort_mean_fitness()
        for robot in self._robots:
            print("ID: {}; Mean fitness: {}; Age: {}".format(robot.id, robot.mean_fitness, robot.age))
