import networkx as nx
import numpy as np


class NetGraph(nx.Graph):
    def __init__(self):
        super(NetGraph, self).__init__()
        self.center_y = 0.0  # Y coordinate center position
        self.distance_x = 5.0  # Distance in x-direction of each layer
        self.distance_y = 1.0  # Distance in y-direction of each node

        self.num_layers = 0
        self.layers = []
        self.edge_labels = {}

    def update_nodes_in_layer(self, layer_n, node_values):
        layer = self.layers[layer_n]
        n_nodes = len(layer['names'])
        if len(node_values) != n_nodes:
            raise ValueError("Wrong number of node values given. Expected " + str(n_nodes) +
                             " got " + str(len(node_values)) + ".")
        for i, name in enumerate(layer['names']):
            layer['labels'][name] = "{:.2f}".format(node_values[i])

        # Change color of output node with highest value
        if layer_n == self.num_layers-1:
            layer['colors'] = ['r']*len(node_values)
            layer['colors'][np.argmax(node_values)] = 'b'

    def draw_graph(self):
        # Put all labels into one list
        labels = {}
        positions = {}
        colors = ''
        for layer in self.layers:
            labels.update(layer['labels'])
            positions.update(layer['positions'])
            colors += "".join(layer['colors'])
            # Draw nodes for each layer separately to be able to set colors correctly
            nx.draw_networkx_nodes(self,
                                   pos=positions,
                                   node_size=600,
                                   node_color="".join(layer['colors']),
                                   nodelist=layer['names'])

        nx.draw_networkx_labels(self, pos=positions, labels=labels, font_size=10)
        nx.draw_networkx_edges(self, pos=positions)
        nx.draw_networkx_edge_labels(self, pos=positions, edge_labels=self.edge_labels,
                                     label_pos=0.15, font_size=8)

    def add_layer(self, name, node_values, edge_values=None):

        # Specify relative positions of nodes
        top_y = self.center_y + len(node_values) * self.distance_y / 2
        pos_x = self.num_layers * self.distance_x

        new_labels = {}
        new_positions = {}

        name_array = []
        color_list = []
        # Add nodes
        for j in xrange(len(node_values)):
            # Set a unique name for the node
            node_name = name+str(j)
            name_array.append(node_name)

            # Create new node dict
            new_labels[node_name] = "{:.2f}".format(node_values[j])
            pos_y = top_y - j*self.distance_y
            new_positions[node_name] = (pos_x, pos_y)
            color_list.append('r')  # Make all modes red by default

            # Add the node
            self.add_node(node_name)

        self.layers.append({'names': name_array,
                            'labels': new_labels,
                            'positions': new_positions,
                            'colors': color_list})

        # Add edges
        if self.num_layers > 0:
            for i in xrange(edge_values.shape[0]):
                in_name = self.layers[-2]['names'][i]
                for j in xrange(edge_values.shape[1]):
                    out_name = self.layers[-1]['names'][j]
                    edge_tuple = (in_name, out_name)
                    self.add_edge(*edge_tuple)
                    self.edge_labels[edge_tuple] = "{:.2f}".format(edge_values[i][j])

        self.num_layers += 1
