import logging
from martai import Algorithm
from martai import Population
import gym

gym.undo_logger_setup()
logging.basicConfig(level=logging.INFO, format='[%(levelname)s]: %(message)s')
gym.logger.setLevel(logging.WARN)

# logging.basicConfig(level=logging.INFO, format='[%(levelname)s]: %(message)s')

# Neural net parameters
net_params = {'n_inputs': 4,  # Number of inputs to the neural network
              'n_hidden': 5,  # Number of nodes in the hidden layer
              'n_outputs': 2}  # Number of outputs of the neural network

# Population parameters
pop_init = {'n_individuals': 100,  # Number of individuals in the population
            'gene_min': -10.0,  # Minimum start value of the genes (weights)
            'gene_max': 10.0}  # Maximum start value of the genes (weights)

pop_mutate = {'rate': 0.05,  # Rate of mutation
              'strength': 10.0}  # Strength of mutation (the maximum change of the mutated gene)

pop_evolve = {'remove_rate': 0.1,  # Part of the population that will be replaced during selection
              'parent_rate': 0.2,  # Part of new individuals that will be created as a combination of two parents
              'crossover_rate': 0.8,  # Chance for a gene to be inherited from parent 1
              'strategy': Population.SINGLE_PARENT}  # Evolution strategy

# Put all population parameters into a single dictionary
pop_params = {'init': pop_init,
              'mutate': pop_mutate,
              'evolve': pop_evolve}

# Repetition parameters
rep_params = {'n_epochs': 20,  # Number of epochs/generations to be run
              'n_repetitions': 1,  # Number of repetitions each scene is run for each epoch and individual
              'n_timesteps': 200}  # Maximum number of timestep of environment

Algorithm.run_algorithm("CartPole-v0", net_params, pop_params, rep_params, epochs_between_render=2)
