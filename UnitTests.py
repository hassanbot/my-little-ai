import copy
import math
import unittest

from martai import Individual
from martai import Population
from martai import NeuralNet
from martai import Robot


class IndividualTest(unittest.TestCase):
    def test_constructor(self):
        ind = Individual(5, min_value=-1, max_value=1)
        for gene in ind.m_genome:
            self.assertGreaterEqual(gene, -1)
            self.assertLessEqual(gene, 1)

    def test_mutate(self):
        max_strength = 100
        strength_increment = 1
        num_genes = 100

        for strength in xrange(1, max_strength, strength_increment):
            ind = Individual(num_genes)
            old_genome = []
            for gene in ind.m_genome:
                old_genome.append(gene)

            ind.mutate(0.5, strength)

            for i in xrange(0, len(ind.m_genome)):
                increase = ind.m_genome[i] / old_genome[i]
                self.assertGreaterEqual(increase, -strength + 1)
                self.assertLessEqual(increase, strength + 1)


class PopulationTest(unittest.TestCase):
    def setUp(self):
        self.num_genes = 100
        self.parents = Population(2, num_genes=self.num_genes, crossover_frequency=0.5)
        self.pop10 = Population(10, num_genes=self.num_genes, crossover_frequency=0.5)

    def assertNoneEqual(self, individual1, individual2):
        for g in xrange(self.num_genes):
            self.assertNotEqual(individual1.m_genome[g], individual2.m_genome[g])

    def assertFromParent(self, child, parents):
        for g in xrange(self.num_genes):
            parent_genes = []
            for parent in parents:
                parent_genes.append(parent.m_genome[g])
            self.assertIn(child.m_genome[g], parent_genes)

    def test_constructor(self):
        pop = Population(100, 100)
        self.assertEqual(len(pop.m_individuals), 100)
        for ind in pop.m_individuals:
            self.assertEqual(len(ind.m_genome), 100)

    def test_addchildren(self):
        self.parents.add_child(self.parents[0], self.parents[1])
        self.parents.add_child(self.parents[0])
        self.parents.add_child(self.parents[1])
        self.parents.add_child()

        self.assertListEqual(self.parents[3].m_genome, self.parents[0].m_genome)
        self.assertListEqual(self.parents[4].m_genome, self.parents[1].m_genome)
        self.assertFromParent(self.parents[2], [self.parents[0], self.parents[1]])
        for p in xrange(4):
            self.assertNoneEqual(self.parents[5], self.parents[p])

    def test_evolve_random(self):
        """Test removing 5 individuals and replacing them with random ones"""
        pop_old = copy.deepcopy(self.pop10)
        self.pop10.evolve(5, strategy=Population.RANDOM)
        for p in xrange(5, 10):
            self.assertNoneEqual(pop_old[p], self.pop10[p])

    def test_evolve_one_parent(self):
        """Test removing 5 individuals and replacing them with new individuals as children from a single parent"""
        self.pop10.evolve(5, strategy=Population.SINGLE_PARENT, mutate_rate=0.0, child_rate=1.0)
        for i in xrange(5):
            self.assertListEqual(self.pop10[i].m_genome, self.pop10[5 + i].m_genome)

    def test_evolve_two_parents_one_child(self):
        """Test removing 4 individuals and adding them as combinations of the 5 best parents"""
        self.pop10.evolve(4, strategy=Population.TWO_PARENTS_ONE_CHILD, mutate_rate=0.0, child_rate=1.0)
        for i in xrange(4):
            self.assertFromParent(self.pop10[6 + i], [self.pop10[i], self.pop10[i + 1]])

    def test_evolve_invalid_strategy(self):
        self.assertRaises(ValueError, self.pop10.evolve, 5, strategy="no_strategy")

    def test_inheritance(self):
        self.parents.add_child(self.parents.m_individuals[0], self.parents.m_individuals[1])

        g_parent1 = self.parents.m_individuals[0].m_genome
        g_parent2 = self.parents.m_individuals[1].m_genome
        g_child = self.parents.m_individuals[2].m_genome

        for g in xrange(self.num_genes):
            self.assertIn(g_child[g], [g_parent1[g], g_parent2[g]])

    def test_mutation(self):
        num_individuals = 10
        population = Population(num_individuals, self.num_genes)
        old_population = copy.deepcopy(population)
        population.mutate(rate=1.0)
        for p in xrange(num_individuals):
            self.assertNotEqual(population.m_individuals[p], old_population.m_individuals[p])

    def test_sort(self):
        num_individuals = 5
        population = Population(num_individuals, self.num_genes)
        population[0].m_fitness = 0
        population[1].m_fitness = 3
        population[2].m_fitness = 1
        population[3].m_fitness = 2
        population[4].m_fitness = 4
        population.sort()
        self.assertEqual(population[0].m_fitness, 4)
        self.assertEqual(population[1].m_fitness, 3)
        self.assertEqual(population[2].m_fitness, 2)
        self.assertEqual(population[3].m_fitness, 1)
        self.assertEqual(population[4].m_fitness, 0)

    def test_update(self):
        num_individuals = 21
        population = Population(num_individuals, self.num_genes)
        for i in xrange(num_individuals):
            population[i].m_fitness = i
        population.sort()
        population_original = copy.deepcopy(population)

        # Test copy
        self.assertEqual(population, population_original)
        for i in xrange(num_individuals):
            self.assertEqual(population[i], population_original[i])

        # Test evolve without mutation
        population.evolve(0, mutate_rate=0.0)
        self.assertEqual(len(population.m_individuals), num_individuals)
        for i in xrange(num_individuals):
            self.assertEqual(population[i], population_original[i])

        # Test evolve with full mutation
        population.evolve(0, mutate_rate=1.0, mutate_strength=10.0)
        for i in xrange(num_individuals):
            self.assertNotEqual(population[i], population_original[i])

        # Test inheritance
        population.sort()
        num_remove = 2
        population.evolve(num_remove, mutate_rate=0.0, strategy=Population.TWO_PARENTS_ONE_CHILD)
        for i in xrange(int(math.ceil(num_remove / 2.0))):
            num_equal = 0
            child = population[num_individuals - int(math.ceil(num_remove / 2.0)) - 1 + i]
            parent1 = population[i]
            parent2 = population[i + 1]
            for g in xrange(self.num_genes):
                if child.m_genome[g] == parent1.m_genome[g] \
                        or child.m_genome[g] == parent2.m_genome[g]:
                    num_equal += 1
            self.assertEqual(num_equal, self.num_genes)


class NeuralNetTest(unittest.TestCase):
    def test_constructor(self):
        num_inputs = 5
        num_hidden = 20
        num_outputs = 3
        net = NeuralNet(num_inputs, num_hidden, num_outputs)

        self.assertEqual(net.m_numinputs, num_inputs + 1)
        self.assertEqual(net.m_numhidden, num_hidden + 1)
        self.assertEqual(net.m_numoutputs, num_outputs)

    def test_feedforward(self):
        num_inputs = 5
        num_hidden = 20
        num_outputs = 3
        net = NeuralNet(num_inputs, num_hidden, num_outputs)
        net.feed_forward([1, 2, 3, 4, 5])

        for output in net.m_output:
            self.assertLessEqual(output, 1.0)
            self.assertGreaterEqual(output, 0.0)
        for hidden in net.m_hidden:
            self.assertLessEqual(hidden, 1.0)
            self.assertGreaterEqual(hidden, -1.0)

    def test_setweights(self):
        num_inputs = 3
        num_hidden = 2
        num_outputs = 4

        net = NeuralNet(num_inputs, num_hidden, num_outputs)
        # weights = [1, 2, ..., 23, 24]
        weights = []
        num_weights = (num_inputs + 1) * num_hidden + (num_hidden + 1) * num_outputs
        for i in xrange(num_weights):
            weights.append(i + 1)

        net.set_weights(weights)

        # m_wi[input][hidden]
        self.assertEqual(20, net.m_wi[0][0])
        self.assertEqual(19, net.m_wi[0][1])
        self.assertEqual(18, net.m_wi[1][0])
        self.assertEqual(17, net.m_wi[1][1])
        self.assertEqual(16, net.m_wi[2][0])
        self.assertEqual(15, net.m_wi[2][1])
        self.assertEqual(14, net.m_wi[3][0])
        self.assertEqual(13, net.m_wi[3][1])
        # m_w0[hidden][output]
        self.assertEqual(12, net.m_wo[0][0])
        self.assertEqual(11, net.m_wo[0][1])
        self.assertEqual(10, net.m_wo[0][2])
        self.assertEqual(9, net.m_wo[0][3])
        self.assertEqual(8, net.m_wo[1][0])
        self.assertEqual(7, net.m_wo[1][1])
        self.assertEqual(6, net.m_wo[1][2])
        self.assertEqual(5, net.m_wo[1][3])
        self.assertEqual(4, net.m_wo[2][0])
        self.assertEqual(3, net.m_wo[2][1])
        self.assertEqual(2, net.m_wo[2][2])
        self.assertEqual(1, net.m_wo[2][3])


class RobotTest(unittest.TestCase):
    def setUp(self):
        # Neural net parameters
        net_params = {'n_inputs': 4,  # Number of inputs to the neural network
                      'n_hidden': 5,  # Number of nodes in the hidden layer
                      'n_outputs': 2}  # Number of outputs of the neural network

        # Population parameters
        pop_init = {'n_individuals': 10,  # Number of individuals in the population
                    'gene_min': -10.0,  # Minimum start value of the genes (weights)
                    'gene_max': 10.0}  # Maximum start value of the genes (weights)

        pop_mutate = {'rate': 0.05,  # Rate of mutation
                      'strength': 10.0}  # Strength of mutation (the maximum change of the mutated gene)

        pop_evolve = {'remove_rate': 0.5,  # Part of the population that will be replaced during selection
                      'parent_rate': 0.2,
                      # Part of new individuals that will be created as a combination of two parents
                      'crossover_rate': 0.8,  # Chance for a gene to be inherited from parent 1
                      'strategy': Population.SINGLE_PARENT}  # Evolution strategy

        # Put all population parameters into a single dictionary
        pop_params = {'init': pop_init,
                      'mutate': pop_mutate,
                      'evolve': pop_evolve}

        # Repetition parameters
        rep_params = {'n_epochs': 10,  # Number of epochs/generations to be run
                      'n_repetitions': 10,  # Number of repetitions each scene is run for each epoch and individual
                      'n_timesteps': 200}  # Maximum number of timestep of environment

        # Intitialize population
        n_individuals = pop_init['n_individuals']
        n_weights = (net_params['n_inputs'] + 1) * net_params['n_hidden'] + \
                    (net_params['n_hidden'] + 1) * net_params['n_outputs']

        population = Population(n_individuals, n_weights,
                                crossover_frequency=pop_evolve['crossover_rate'],
                                gene_min=pop_init['gene_min'], gene_max=pop_init['gene_max'])

        robots = []
        for individual in population.m_individuals:
            robots.append(Robot("CartPole-v0", net_params, pop_params, rep_params, individual=individual))

        for i in xrange(pop_init['n_individuals']):
            self.assertEqual(population[i], robots[i].individual)

        population.evolve(pop_init['n_individuals'], strategy=Population.RANDOM)

        for i in xrange(pop_init['n_individuals']):
            self.assertEqual(population[i], robots[i].individual)
            self.assertListEqual(population[i].m_genome, robots[i].individual.m_genome)

if __name__ == '__main__':
    unittest.main()
