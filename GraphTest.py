import random
import time

from martai import NeuralNet

n_inputs = 4
n_hidden = 5
n_outputs = 2
net = NeuralNet(n_inputs, n_hidden, n_outputs, visualize=True)

for epoch in xrange(5):
    action = []
    for i in xrange(n_inputs):
        action.append(random.uniform(-2, 2))
    net.feed_forward(action)
    net.draw_graph()

    print("Finished iteration " + str(epoch) + ".")
    time.sleep(0.5)
